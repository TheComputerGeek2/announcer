package ca.shadownode.announcer.sponge.commands;

import ca.shadownode.announcer.sponge.AnnouncerPlugin;
import ca.shadownode.announcer.sponge.utils.Utilities;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;

public class AnnounceCommand implements CommandExecutor {

    private final AnnouncerPlugin plugin;
    
    public AnnounceCommand(AnnouncerPlugin plugin) {
        this.plugin = plugin;
    }
    
    public void register() {
        CommandSpec announce = CommandSpec.builder()
            .permission("announcer.command.announce")
            .description(Text.of("Announcer command."))
            .arguments(
                GenericArguments.remainingJoinedStrings(Text.of("message"))
            )
            .executor(this)
            .build();
        Sponge.getCommandManager().register(plugin, announce, "announce");
    }
    
    @Override
    public CommandResult execute(CommandSource src, CommandContext context) {
        String message = context.<String>getOne("message").get();
        Sponge.getServer().getBroadcastChannel().send(Utilities.parseMessage(message));
        return CommandResult.success();
    }
    
}
