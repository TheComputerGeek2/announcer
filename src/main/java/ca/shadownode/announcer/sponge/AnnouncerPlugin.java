package ca.shadownode.announcer.sponge;

import ca.shadownode.announcer.sponge.config.Configuration;
import ca.shadownode.announcer.sponge.commands.AnnounceCommand;
import ca.shadownode.announcer.sponge.commands.AnnouncerReloadCommand;
import ca.shadownode.announcer.sponge.tasks.AnnounceTask;
import com.google.inject.Inject;
import java.io.File;
import java.io.IOException;
import org.slf4j.Logger;

import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.game.state.GameStoppingServerEvent;
import org.spongepowered.api.plugin.Plugin;

@Plugin(id = "announcer")
public class AnnouncerPlugin {

    @Inject
    private Logger logger;
    
    @Inject
    @DefaultConfig(sharedRoot = true)
    public File defaultConfig;
    
    public Cause pluginCause; 
    public Configuration config;
    
    public AnnounceTask announceTask;
    
    @Listener(order = Order.POST)
    public void onStart(GameStartedServerEvent event) throws IOException {
        config = new Configuration(this);
        new AnnounceCommand(this).register();
        new AnnouncerReloadCommand(this).register();
        announceTask = new AnnounceTask(this);
        announceTask.start();
    }
    
    @Listener(order = Order.POST)
    public void onStop(GameStoppingServerEvent event) throws IOException {
        if(getConfig().announcerEnabled && announceTask.isRunning()) {
            announceTask.stop();
        }
    }
    
    public Configuration getConfig() {
        return config;
    }
 
    public Logger getLogger() {
        return logger;
    }

}
