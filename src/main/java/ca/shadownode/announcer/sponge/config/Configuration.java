package ca.shadownode.announcer.sponge.config;

import ca.shadownode.announcer.sponge.AnnouncerPlugin;
import com.google.common.reflect.TypeToken;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;

public class Configuration {

    private final AnnouncerPlugin plugin;   
    
    private ConfigurationLoader loader;
    private ConfigurationNode config;

    public boolean announcerEnabled;
    public int announcerInterval;
    public String announcerPrefix;
    public List<String> announcerMessages;

    public Configuration(AnnouncerPlugin plugin) {
        this.plugin = plugin;
        loadConfig();
    }

    public void loadConfig() {
        try {
            loader = HoconConfigurationLoader.builder().setFile(plugin.defaultConfig).build();
            saveDefaultConfig();
            config = loader.load();
            announcerEnabled = config.getNode("Announcer", "Enabled").getBoolean();
            announcerInterval = config.getNode("Announcer", "Interval").getInt();
            announcerPrefix = config.getNode("Announcer", "Prefix").getString();
            announcerMessages = config.getNode("Announcer", "Messages").getList(TypeToken.of(String.class));
        } catch (IOException | ObjectMappingException ex) {
            plugin.getLogger().error("The default configuration could not be loaded!", ex);
        }
    }
    
    public void saveDefaultConfig() {
        if (!plugin.defaultConfig.exists()) {
            try {
                plugin.defaultConfig.createNewFile();
                config = loader.load();
                config.getNode("Announcer", "Enabled").setValue(false);
                config.getNode("Announcer", "Interval").setValue(5);
                config.getNode("Announcer", "Prefix").setValue("&8[&5ShadowNode&8]&r");
                config.getNode("Announcer", "Messages").setValue(new TypeToken<List<String>>() {
                }, Arrays.asList(
                        "First Message",
                        "Second Message"
                ));
                loader.save(config);
            } catch (IOException | ObjectMappingException ex) {
                plugin.getLogger().error("The default configuration could not be created!", ex);
            }
        }
    }

}
