package ca.shadownode.announcer.sponge.commands;

import ca.shadownode.announcer.sponge.AnnouncerPlugin;
import ca.shadownode.announcer.sponge.utils.Utilities;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;

public class AnnouncerReloadCommand implements CommandExecutor {

    private final AnnouncerPlugin plugin;
    
    public AnnouncerReloadCommand(AnnouncerPlugin plugin) {
        this.plugin = plugin;
    }
    
    public void register() {
        CommandSpec announcerreload = CommandSpec.builder()
            .permission("announcer.command.announcerreload")
            .description(Text.of("Announcer Reload command."))
            .executor(this)
            .build();
        Sponge.getCommandManager().register(plugin, announcerreload, "announcerreload");
    }
    
    @Override
    public CommandResult execute(CommandSource src, CommandContext context) {
        if(plugin.announceTask != null) {
            plugin.announceTask.stop();
        }
        plugin.getConfig().loadConfig();
        if(plugin.announceTask != null) {
            plugin.announceTask.start();
        }
        src.sendMessage(Utilities.parseMessage("&8[&5ShadowNode&8]&a Reloaded Announcer Config."));
        return CommandResult.success();
    }
    
}
