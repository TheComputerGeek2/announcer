package ca.shadownode.announcer.sponge.tasks;

import ca.shadownode.announcer.sponge.AnnouncerPlugin;
import ca.shadownode.announcer.sponge.utils.Utilities;
import java.util.concurrent.TimeUnit;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.scheduler.Scheduler;
import org.spongepowered.api.scheduler.Task;

public class AnnounceTask {

    private final AnnouncerPlugin plugin;
    
    private boolean running;
    private int lastAnnouncement = 0;
    private Task task;
    
    public AnnounceTask(AnnouncerPlugin plugin) {
        this.plugin = plugin;
    }
        
    public Boolean isRunning() {
        return running;
    }

    public void stop() {
        running = false;
        task.cancel();
    }

    public void start() {
        if (!plugin.getConfig().announcerEnabled) {
            return;
        }
        running = true;
        Scheduler scheduler = Sponge.getScheduler();
        Task.Builder taskBuilder = scheduler.createTaskBuilder();
        task = taskBuilder.execute(() -> {
            if (plugin.getConfig().announcerMessages.size() > 0) {
                announce(lastAnnouncement);
                lastAnnouncement++;
            }
            if(lastAnnouncement >= plugin.getConfig().announcerMessages.size()) {
                lastAnnouncement = 0;
            }
        }).async()
            .interval(plugin.getConfig().announcerInterval, TimeUnit.MINUTES)
            .name("AnnouncerTask - Sends out chat announcements.").submit(plugin);
    }

    public void announce(int index) {
        String message = plugin.getConfig().announcerMessages.get(index);
         
        if(message != null) {
            Sponge.getServer().getBroadcastChannel().send(Utilities.parseMessage(plugin.getConfig().announcerPrefix + " "  + message));
        }
    }
}
